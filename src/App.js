import './App.css';
import { Route, Routes, BrowserRouter, Link, Navigate, useNavigate} from 'react-router-dom'
// import PersonIcon from '@mui/icons-material/Person';
import { Person } from '@mui/icons-material';
import { useState } from 'react'
import {useLogout} from './hooks/UseLogout'
import Button from '@mui/material/Button';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import {useAuthContext} from './hooks/UseAuthContext'


//components
import Home from './pages/home/Home'
import About from './pages/about/About'
import Checkout from './pages/checkout/Checkout';
import Login from './pages/login/Login';
import Signup from './pages/signup/Signup';
import Products from './pages/products/Products';
import Product from './pages/products/Product';



function App() {
  const [isHovered, setIsHovered] = useState(false);
  const {logout} = useLogout()
  const {user, authIsReady} = useAuthContext()


  const theme = createTheme({
    status: {
      danger: '#e53e3e',
    },
    palette: {
      primary: {
        main: '#0971f1',
        darker: '#053e85',
      },
      neutral: {
        main: '#ad916e',
        contrastText: '#fff',
      },
    },
  });
   
  const handleMouseEnter = () => {
    setIsHovered(true);    
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };
  return (
    <div className="App">
      {authIsReady && (
      <BrowserRouter>
        <nav>
        <h1><Link className="logo" to="/">RICO JEWELRY</Link></h1>
          <Link to="/about">ABOUT US</Link>
          <Link to="/products">PRODUCTS</Link> 
          <Link to="/checkout">CHECKOUT</Link>
            {user && <p className="user">HELLO, {user.displayName.toUpperCase() } !</p>}
        </nav>
          {user && <ThemeProvider  theme={theme}>
              <Button  onClick={ logout}
              color="neutral" className="logout"  variant="outlined">LOGOUT</Button>
          </ThemeProvider>}
          {!user && <div  onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
            <Person className="person"/>
            <br />
          {isHovered && <Link className="login" to="/login">login</Link>}
          <br />
          {isHovered && <Link className="signup" to="/signup">signup</Link>}
          </div>}
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/checkout" element={<Checkout />} />
          <Route path="/login" element={user ? <Navigate to='/'/> : <Login />} />
          <Route path="/signup" element={user ? <Navigate to='/'/> :<Signup />} />
          <Route path="/products" element={<Products />} />
          <Route path="/products/:id/*" element={<Product />} />
        </Routes>
      </BrowserRouter>
      )}
    </div>
  );
}

export default App;
