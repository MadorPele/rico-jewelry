import { useState, useEffect } from 'react'
import { projectAuth } from '../firebase/config'
import { useAuthContext } from './UseAuthContext'

export const useSignup = () => {
    const [error, setError] = useState(null)
    const [isPending, setIsPending] = useState(false)
    const { dispatch } = useAuthContext()
    const [isCancelled, setIsCncelled] = useState(false)


    const signup = async (email, password, displayName) => {
        setError(null)
        setIsPending(true)

        try{
            const res = await projectAuth.createUserWithEmailAndPassword(email, password)
            console.log(res)
            
            if(!res) {
                throw new Error('Could not complete signup')
            }

            await res.user.updateProfile({ displayName })

            dispatch ({type:'LOGIN', payload: res.user})

            if(!isCancelled) {
                setError(null)
                setIsPending(false)
            }
        }
       catch (err) {
        if(!isCancelled) {
            setError(err)
            setIsPending(false)
            console.log(err.message)
        }
    }
    }
    useEffect(() => {
        return setIsCncelled(true)
    }, [])

    return {error, isPending, signup}
}