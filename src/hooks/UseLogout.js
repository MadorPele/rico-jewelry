import { useState, useEffect } from 'react'
import { projectAuth } from '../firebase/config'
import { useAuthContext } from './UseAuthContext'

export const useLogout = () => {
  const [error, setError] = useState(null)
  const [isPending, setIsPending] = useState(false)
  const { dispatch } = useAuthContext()
  const [isCancelled, setIsCncelled] = useState(false)

  const logout = async () => {
    setError(null)
    setIsPending(true)
    console.log('hello')
    

    try {
        await projectAuth.signOut()
        dispatch({type: 'LOGOUT'})

        if(!isCancelled) {
            setError(null)
            setIsPending(false)
        }
    }
    catch (err) {
        if(!isCancelled) {
            setError(err)
            setIsPending(false)
            console.log(err.message)
        }
    }
  }

  useEffect(() => {
      return setIsCncelled(true)
  }, [])
  return { logout, error, isPending }
}