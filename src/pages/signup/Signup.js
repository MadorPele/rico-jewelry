import { useState } from 'react'
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Button from '@mui/material/Button';
import {useSignup} from '../../hooks/UseSignup'



// styles
import './Signup.css'

export default function Signup() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [displayName, setDisplayName] = useState('')
  const {error, isPending, signup} = useSignup()

  const handleSubmit = (e) => {
    e.preventDefault()
    signup(email, password, displayName)
  }

  const theme = createTheme({
    status: {
      danger: '#e53e3e',
    },
    palette: {
      primary: {
        main: '#0971f1',
        darker: '#053e85',
      },
      neutral: {
        main: '#ad916e',
        contrastText: '#fff',
      },
    },
  });

  return (
    <form onSubmit={handleSubmit} className='signup-form'>
      <h2>sign up</h2>
      <label>
        <span>email:</span>
        <input 
          type="email" 
          onChange={(e) => setEmail(e.target.value)} 
          value={email}
        />
      </label>
      <label>
        <span>password:</span>
        <input 
          type="password" 
          onChange={(e) => setPassword(e.target.value)} 
          value={password} 
        />
      </label>
      <label>
        <span>display name:</span>
        <input 
          type="text" 
          onChange={(e) => setDisplayName(e.target.value)}
          value={displayName}
        />
      </label>
      { !isPending &&    <ThemeProvider theme={theme}>
            <Button onClick={handleSubmit}  color="neutral" className="log" variant="outlined">LOGIN</Button>
        </ThemeProvider> }
      { isPending &&    <ThemeProvider theme={theme}>
            <Button onClick={handleSubmit} disabled color="neutral" className="log" variant="outlined">LOGIN</Button>
        </ThemeProvider> }
      { error && <p>{error}</p> }
   
    </form>
  )
}