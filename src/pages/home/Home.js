import './Home.css'
import Button from '@mui/material/Button';

import homepic from '../../images/homee.jpeg'
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';



export default function Home() {
  const navigate = useNavigate()

const theme = createTheme({
  status: {
    danger: '#e53e3e',
  },
  palette: {
    primary: {
      main: '#0971f1',
      darker: '#053e85',
    },
    neutral: {
      main: '#ad916e',
      contrastText: '#fff',
    },
  },
});

    return (
        <div>
          <img src={homepic} alt="jimg" />
          <ThemeProvider theme={theme}>
          <Button color="neutral" className="shop" variant="outlined" onClick={() => navigate('/products')}>SHOP NOW</Button>
          </ThemeProvider>
        </div>
    )
}
