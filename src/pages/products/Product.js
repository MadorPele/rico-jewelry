import {useParams, Route, Routes, useNavigate} from 'react-router-dom'
import {projecrFirstore} from '../../firebase/config'
import { useState, useEffect } from 'react'
import './Product.css'
import Cartopen from './Cartopen'
import { useDocument } from '../../hooks/UseDocument'
import {useAuthContext} from '../../hooks/UseAuthContext'
import firebase from 'firebase/app';
import 'firebase/firestore';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Cordion from './Cordion'

const sizes= [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60]

export default function Product({ jewelry }) {
    const { id } = useParams()
    const [theSize, setTheSize] = useState('')
    const [toOpen, setToOpen] = useState(false)
    const [eData, setEData] = useState('')
    const navigate = useNavigate()
    const {user} = useAuthContext()
    const { document, error } = useDocument('products', id)

    const theme = createTheme({
        status: {
          danger: '#e53e3e',
        },
        palette: {
          primary: {
            main: '#0971f1',
            darker: '#053e85',
          },
          neutral: {
            main: '#ad916e',
            contrastText: '#fff',
          },
        },
      });
  
   const addToCart = async (userId, productId) => {
       if(toOpen) {
    const docRef = projecrFirstore.collection('users').doc(userId);

    const documentSnapshot = await docRef.get();
    if (documentSnapshot.exists) {
        try {
            console.log('lslaalssl');
            
          await docRef.update({
            cart: firebase.firestore.FieldValue.arrayUnion(productId),
        });
          console.log('Product added to cart successfully!');
        } catch (error) {
          console.error('Error adding product to cart:', error);
        }
            navigate(`/products/${id}/cartopen`)
            setToOpen(false)
    } else {
        console.log('kkkkkkk');

        docRef.set({
            cart: firebase.firestore.FieldValue.arrayUnion(productId)
    })
} 
}else {
    alert('YOU HAVE TO CHOOSE A SIZE')

}        
}
    
    if (error) {
      return <div className="error">{error}</div>
    }
    if (!document) {
      return <div className="loading">Loading...</div>
    }
    
    if (jewelry === 0) {
        return <div>no products to load..</div>
    }
    console.log(toOpen);
    

    const wichOne = (size) => {
        setTheSize(size)
        setToOpen(true)
    }

    const added = () => {
        if(user) {
            addToCart(user.uid, document.id)
        }
    }
  
    return (
        <div className="product">
             <p style={{fontSize: '3vh'}}>{document.name}</p>
            <div >
                <div className="sizese">
                <p style={{fontSize: '1.5vh'}}>CHOOSE YOUR SIZE: {theSize}</p>
           <div className="sizes">{sizes.map((size, index) => (
               <p key={index} className="op" onClick={()=> wichOne(size)}>{size}</p>
               ))}</div>
               <p>{document.price}₪</p>
           </div>
           <ThemeProvider theme={theme}>
            <Button onClick={added}  color="neutral" className="ad" variant="outlined">ADD TO CART</Button>
        </ThemeProvider>
        <Cordion data={document}/>
        <h2>JEWELRY DESCRIPTION:</h2>
        <p>{document.description}</p>
           </div>
            <Routes> <Route path="cartopen" element={<Cartopen data={document} tsize={theSize} id={id} />} /></Routes>   
        </div>
    )
}
