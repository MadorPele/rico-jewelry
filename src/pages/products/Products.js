import './Products.css'
import {projecrFirstore} from '../../firebase/config'
import { useEffect, useState } from 'react'
// import Product from './Produc'
import { useNavigate} from 'react-router-dom'

import pic1 from '../../images/pic1.jpeg'
import pic2 from '../../images/pic2.jpeg'
 
const images = [pic1, pic2]


export default function Products() {

    const [data, setData] = useState(null)
    const [isPending, setIsPending] = useState(false)
    const [error, setError] = useState(false)
    const [isHovered, setIsHovered] = useState(null);
    const [pict, setPict] = useState('')
    const navigate = useNavigate()

    useEffect(() => {
        projecrFirstore.collection('products').get().then(snapshot => {
            if(snapshot.empty) {
                setError('no products to load')
            } else {
                let res = []
                snapshot.docs.map(pro => {
                    res.push({...pro.data(), id:pro.id})
                })
                setData(res)
                console.log(data);
                
                setIsPending(false)
            }
        })
    }, [])

    const handleJewelryClick = (id, i) => {
        navigate(`/products/${id}`);
        // setpict(images[i]);
        
        // return (<img src={images[i]} src="img" />)
        
        
      };
    return (
        <div className="jewelrys">
            {error && <p>{error}</p>}
            {isPending && <p>Loading..</p>}
            {data && data.map((je, index) => (
              <div className="jewelry" key={je.id} onClick={() => handleJewelryClick(je.id, index)}>
                  <h2 className="item"
                  onMouseEnter={() => setIsHovered(je.name)}
                  onMouseLeave={() => setIsHovered(null)}
                    >{je.name}</h2>
                  <img src={images[index]} alt="pic" />            
                  {isHovered === je.name && <p>{je.price}₪</p>}
              </div>
        ))} 
            
        </div>
    )
}
