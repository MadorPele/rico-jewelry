import { Accordion, AccordionItem } from '@szhsin/react-accordion';
import './Cardion.css'
import styles from "./Cardion.css";

export default function Example() {
  return (
      <div className="app">
    <Accordion className="szh-accordion">
      <AccordionItem header="What is Lorem Ipsum?" className="szh-accordion__item">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
        do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </AccordionItem>

      <AccordionItem header="Where does it come from?" className="szh-accordion__item">
        Quisque eget luctus mi, vehicula mollis lorem. Proin fringilla
        vel erat quis sodales. Nam ex enim, eleifend venenatis lectus
        vitae, accumsan auctor mi.
      </AccordionItem>

      <AccordionItem header="Why do we use it?" className="szh-accordion__item">
        Suspendisse massa risus, pretium id interdum in, dictum sit
        amet ante. Fusce vulputate purus sed tempus feugiat.
      </AccordionItem>
    </Accordion>
    </div>
  );
}