import './Cart.css'
import { useState, useEffect } from 'react'
import ClearIcon from '@mui/icons-material/Clear';
import {useNavigate, useParams} from 'react-router-dom'
import {useDocument} from '../../hooks/UseDocument'
import {useAuthContext} from '../../hooks/UseAuthContext'
import {projecrFirstore} from '../../firebase/config'
import DeleteIcon from '@mui/icons-material/Delete';

const products = []

export default function Cartopen(props) {
    const [proNum, setProNum] = useState(0)
    const thePrice = props.data.price
    const navigate = useNavigate()
    const [nPrice, setNPrice] = useState(thePrice)
    const {user} = useAuthContext()
    const { document, error} = useDocument('users', user.uid)
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);
    const { id } = useParams()


  
    const minus = (r) => {
        if (!(proNum === 0))
        setProNum(Number(proNum - 1))
        console.log(proNum);
        setNPrice(Number(proNum * r ))

    }

    const plus = (t) => {  
      setProNum(Number(proNum + 1))
      setNPrice(Number(proNum * t ))  
              
    }

    const exit = () => {
        navigate(`/products/${props.id}`)
    }
    useEffect(() => {
      const fetchProducts = async () => {
        try {
          if (!document || !document.cart) {
            // Handle the case when the document or cart is not available
            setProducts([]);
            setLoading(false);
            return;
          }
    
          const productMap = new Map();
    
          // Loop through the cart items and accumulate the price for duplicate products
          document.cart.forEach((doc) => {
            if (productMap.has(doc)) {
              
              const existingProduct = productMap.get(doc);
              const updatedProduct = {
                ...existingProduct,
                price: existingProduct.price * 2, // Double the price for duplicate products
              };
              productMap.set(doc, updatedProduct);
            } else {
              productMap.set(doc, null); // Use null as a placeholder for yet-to-be-fetched products
            }
          });
    
          const productPromises = Array.from(productMap.keys()).map(async (doc) => {
            const docRef = projecrFirstore.collection('products').doc(doc);
            const snapshot = await docRef.get();
    
            if (snapshot.exists) {
              setProNum(proNum + 1)
              const productData = snapshot.data();
              productMap.set(doc, {
                ...productData,
                price: productData.price * 2, // Double the price for duplicate products
              }); // Replace the placeholder with the fetched product data
            } else {
              console.log(`Product with ID ${doc} does not exist.`);
            }
          });
    
          await Promise.all(productPromises);
    
          const filteredProducts = Array.from(productMap.values()).filter((product) => product !== null);
          setProducts(filteredProducts);
        } catch (error) {
          console.error('Error fetching products:', error);
        } finally {
          setLoading(false);
        }
      };
    
      fetchProducts();
    }, [document]);
    
    if (loading) {
      return <div>Loading...</div>;
    }
    
    const deletei = (p) => {
      const updatedProducts = products.filter((product) => product.id !== p.id)
      setProducts(updatedProducts);
      if (products.length === 0) {
        navigate(`/products/${id}`)
      }
    }
  return (
    <div className="cart">
       < ClearIcon onClick={exit} style={{position: 'relative', right:'6vw', top:'2vh'}}/>
       {products.map((product) => (
         <div style={{position: 'relative', top: '3vh'}} key={product.id}>
           <p style={{fontSize:'1.5vh', position:'relative', right: '5vw', display: 'inline'}}>{product.name}</p>
           <ClearIcon style={{ position:'relative', left: '5vw'}}onClick={()=> deletei(product)} />
         <p>size: {props.tsize}</p>
         <p>{nPrice}₪</p>
          <span  onClick={() => plus(product.price)}>  +  </span>
          <span> {proNum} </span>
          <span onClick={() => minus(product.price)}>  -  </span>
        </div>
      ))}
       
       {/* {document && document.cart.map((doc)=> (
         projectFirestore.collection('products').doc(doc)
       <p>{}</p>
       ))}
       <p>{nPrice}₪</p> */}

    </div>
  )
}