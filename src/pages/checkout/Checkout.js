import './Checkout.css'
import {useAuthContext} from '../../hooks/UseAuthContext'

export default function Checkout() {
    const {user} = useAuthContext()
    return (
        <div>
            {!user && "YOUR CART IS EMPTY"}
        </div>
    )
}
