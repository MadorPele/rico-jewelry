import {useState} from 'react'
import Button from '@mui/material/Button';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import './Login.css'
import {useLogin} from '../../hooks/UseLogin'


export default function Login() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const {login, isPending, error} = useLogin()

    const handleSubmit = (e) => {
        e.preventDefault()
        login(email, password);
      
        
    }

    const theme = createTheme({
        status: {
          danger: '#e53e3e',
        },
        palette: {
          primary: {
            main: '#0971f1',
            darker: '#053e85',
          },
          neutral: {
            main: '#ad916e',
            contrastText: '#fff',
          },
        },
      });
    return (
        <form className="login-form" onSubmit={handleSubmit}>
            <h2>LOGIN</h2>
            <label>
                <span>EMAIL:</span>
                <input
                    type="email"
                    onChange={(e) => setEmail(e.target.value)}
                    required
                    value={email}
                    />
            </label>
            <label>
                <span>PASSWORD:</span>
                <input
                    type="password"
                    onChange={(e) => setPassword(e.target.value)}
                    required
                    value={password}
                />
            </label>
            { !isPending &&    <ThemeProvider theme={theme}>
            <Button onClick={handleSubmit}  color="neutral" className="log" variant="outlined">LOGIN</Button>
        </ThemeProvider> }
      { isPending &&    <ThemeProvider theme={theme}>
            <Button onClick={handleSubmit} disabled color="neutral" className="log" variant="outlined">LOGIN</Button>
        </ThemeProvider> }
      { error && <p>{error}</p> }
      {/* <ThemeProvider theme={theme}>
            <Button onClick={handleSubmit}  color="neutral" className="log" variant="outlined">LOGIN</Button>
        </ThemeProvider> } */}
        </form>
    )
}

