import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyCeC3ph0KubvTIO8_1I5dTkbyDyWXQBglY",
    authDomain: "rico-fa351.firebaseapp.com",
    projectId: "rico-fa351",
    storageBucket: "rico-fa351.appspot.com",
    messagingSenderId: "481472264034",
    appId: "1:481472264034:web:896f5e9c31d7fadd889e3b"
  };

  firebase.initializeApp(firebaseConfig)

  
  const projecrFirstore = firebase.firestore()
  const projectAuth = firebase.auth()
  const projectStorage = firebase.storage()

  export {projecrFirstore, projectAuth, projectStorage}